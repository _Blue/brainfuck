#include <iostream>
#include <fstream>
#include <cstring>
#include "Brainfuck.h"

using std::cout;
using std::cin;
using std::cerr;
using std::endl;
using std::istream;
using std::vector;
using std::ifstream;

constexpr const char* version = "1.0-release";

#ifdef RUN_TEST
bool Start_test();
#endif

static void Help()
{
  cout << "brainfuck " << version
       << ", Blue, 2015. For more infos, checkout http://bluecode.fr" << endl;
  cout << "Usage: brainfuck [-h] [-m memory] [-f file]" << endl;
  cout << endl;
  cout << "Options: " << endl;
  cout << "\t-m count" << endl;
  cout << "\t\tAllow the brainfuck program to use at most count bytes of memory"
       << endl;
  cout << "\t-f file" << endl;
  cout << "\t\tReads brainfuck code from `file' instead of stdin" << endl;
  cout << "\t-h" << endl;
  cout << "\t\tDisplays this message" << endl;
}

// because getopt(3) is not available on windows, we have to do this by hand
static bool ParseArgv(int argc, char** argv, const char*& filename,
                      unsigned long& memsize, bool& help)
{
  filename = nullptr;
  help = false;
  memsize = 0;
  for (int i = 1; i < argc; i++)
  {
    if (strcmp(argv[i], "-f") == 0)
    {
      if (i + 1 == argc || argv[i + 1][0] == '-')
      {
        cerr << "-f: this option requires an argument" << endl;
        return false;
      }
      filename = argv[i + 1];
      i++;
    }
    else if (strcmp(argv[i], "-m") == 0)
    {
      if (i + 1 == argc || argv[i + 1][0] == '-')
      {
        cerr << "-m: this option requires an argument" << endl;
        return false;
      }
      memsize = strtoul(argv[i + 1], nullptr, 10);
      if (memsize == 0)
      {
        cerr << "-m: " << argv[i + 1] << ": invalid memory size" << endl;
        return false;
      }
      i++;
    }
    else if (strcmp(argv[i], "-h") == 0)
    {
      help = true;
    }
    else
    {
      cerr << "Unknown option: \"" << argv[i] << "\"" << endl;
      return false;
    }
  }
  return true;
}

static vector<char> VectorFromStream(istream& stream, bool& eof, bool ignorelf)
{
  vector<char> ret;
  while (!stream.eof())
  {
    char c;
    stream.read(&c, 1);
    if (stream.eof())
    {
      break;
    }
    if (c == '\r')
    {
      continue; // Completely ignore Windows '\r'
    }
    if (c == '\n')
    {
      if (!ignorelf)
      {
        break;
      }
      else
      {
        continue;
      }
    }
    ret.push_back(c);
  }

  eof = stream.eof();
  return ret;
}

int main(int argc, char** argv)
{
#ifdef RUN_TEST
  return !Start_test();
#endif

  bool help;
  unsigned long memsize;
  const char* file;
  if (!ParseArgv(argc, argv, file, memsize, help))
  {
    return 1;
  }

  if (help)
  {
    Help();
    return 0;
  }

  bool eof = false;

  ifstream filestream;
  istream& codestream = filestream;
  if (file != nullptr)
  {
    filestream.open(file);
    if (!filestream)
    {
      cerr << "Fatal: failed to open \"" << file << "\" for reading" << endl;
      return 1;
    }
    codestream.rdbuf(filestream.rdbuf());
  }
  else
  {
    codestream.rdbuf(cin.rdbuf());
  }

  while (!eof)
  {
    vector<char> code = VectorFromStream(codestream, eof, file != nullptr);

    Brainfuck brainfuck(code, memsize);

    if (!brainfuck.Run())
    {
      return 2;
    }
  }

  return 0;
}
