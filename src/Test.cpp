#include<sstream>
#include"Brainfuck.h"

using std::vector;
using std::ostream;
using std::stringstream;
using std::istream;
using std::istringstream;
using std::cout;
using std::endl;

bool Execute(const char *code, const std::string &expectedoutput = "", bool result = true, const std::string &givenintput = "", const std::string &expectederror = "", size_t mem = 0)
{
  vector<char> instructions;
  while (*code != '\0')
  {
    instructions.push_back(*code);
    code++;
  }

  stringstream output;
  stringstream error;
  istringstream input(givenintput);
  Brainfuck brainfuck(instructions, mem, output, input, error);
  if (brainfuck.Run() != result)
  {
    cout << "Test failed, unexpected result" << endl;
    return false;
  }
  if (output.str() != expectedoutput)
  {
    cout << "Test failed, unexpected output" << endl;
    return false;
  }
  if (error.str() != expectederror)
  {
    cout << "Test failed, unexpected error" << endl;
    return false;
  }
  return true;
}

bool Start_test()
{
  //Basic hello world test
  const char *hello = ">+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.>>>++++++++[<++++>-]<.>>>++++++++++[<+++++++++>-]<---.<<<<.+++.------.--------.>>+.";
  if (!Execute(hello, "Hello World!"))
  {
    return false;
  }

  //Fibonnaci test
  const char *fibo = "+++++++++++"
    ">+>>>>++++++++++++++++++++++++++++++++++++++++++++"
    ">++++++++++++++++++++++++++++++++<<<<<<[>[>>>>>>+>"
    "+<<<<<<<-]>>>>>>>[<<<<<<<+>>>>>>>-]<[>++++++++++[-"
    "<-[>>+>+<<<-]>>>[<<<+>>>-]+<[>[-]<[-]]>[<<[>>>+<<<"
    "-]>>[-]]<<]>>>[>>+>+<<<-]>>>[<<<+>>>-]+<[>[-]<[-]]"
    ">[<<+>>[-]]<<<<<<<]>>>>>[+++++++++++++++++++++++++"
    "+++++++++++++++++++++++.[-]]++++++++++<[->-<]>++++"
    "++++++++++++++++++++++++++++++++++++++++++++.[-]<<"
    "<<<<<<<<<<[>>>+>+<<<<-]>>>>[<<<<+>>>>-]<-[>>.>.<<<"
    "[-]]<<[>>+>+<<<-]>>>[<<<+>>>-]<<[<+>-]>[<+>-]<<<-]";

  if (!Execute(fibo, "1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89"))
  {
    return false;
  }

  //Invalid code
  if (!Execute("Invalid code", "", false, "", "Fatal: Invalid instruction: \"I\"\nAt code position: 0, at memory position: 0\n"))
  {
    return false;
  }

  //Memory overrun
  if (!Execute(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>", "", false, "", "Fatal: Program out of memory (needs more than 2 bytes)\nAt code position: 1, at memory position: 2\n", 2))
  {
    return false;
  }

  //User input
  if (!Execute(",++.", "C", true, "A"))
  {
    return false;
  }

  //Invalid loop end
  if (!Execute("++><]", "", false, "", "Fatal: Ending a loop while not in a loop\nAt code position: 4, at memory position: 0\n"))
  {
    return false;
  }

  //Missing loop end
  if (!Execute("[>+", "", false, "", "Fatal: Unterminated loop\nAt code position: 3, at memory position: 0\n"))
  {
    return false;
  }

  //Negative address access
  if (!Execute("<", "", false, "", "Fatal: Access to a negative address\nAt code position: 0, at memory position: 0\n"))
  {
    return false;
  }
  return true;
}
