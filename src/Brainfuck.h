#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <stack>

#define BASE_MEM_SIZE 4096
class Brainfuck
{
public:
  Brainfuck(const std::vector<char> &instructions, size_t maxmem = 0, std::ostream &output = std::cout, std::istream &input = std::cin, std::ostream &errorstream = std::cerr);
  bool Run();

private:
  bool ExecuteNext();
  bool ExtendMemory();
  bool FindLoopEnd();
  bool MoveMemptr(bool forward);
  void Error(const std::string &message);

  const std::vector<char> &_instructions;
  std::vector <unsigned char> _memory;
  std::ostream &_errorstream;
  std::ostream &_output;
  std::istream &_input;
  std::stack<size_t> _stack;
  size_t _instructionptr;
  size_t _memptr;
  size_t _maxmem;
};