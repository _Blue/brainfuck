#include <algorithm>
#include "Brainfuck.h"

using std::vector;
using std::ostream;
using std::istream;
using std::stack;
using std::string;
using std::endl;

Brainfuck::Brainfuck(const vector<char> &instructions, size_t maxmem, ostream &output, istream &input, std::ostream &errorstream) :
  _instructions(instructions),
  _errorstream(errorstream),
  _output(output),
  _input(input)
{
  _maxmem = maxmem;
}

bool Brainfuck::Run()
{
  _memptr = 0;
  _instructionptr = 0;
  if (_maxmem != 0)
  {
    _memory.resize(std::min(static_cast<size_t>(BASE_MEM_SIZE), _maxmem));//Make sure default memory size won't override memory limit
  }
  else
  {
    _memory.resize(BASE_MEM_SIZE);
  }
  while (_instructionptr < _instructions.size())
  {
    if (!ExecuteNext())
    {
      return false;
    }
  }
  return true;
}

bool Brainfuck::MoveMemptr(bool forward)
{
  if (forward)
  {
    _memptr++;
    if (_memptr == _memory.size())
    {
      return ExtendMemory();
    }
  }
  else
  {
    if (_memptr == 0)
    {
      Error("Access to a negative address");
      return false;
    }
    _memptr--;
  }
  return true;
}

bool Brainfuck::FindLoopEnd()
{
  size_t depth = 1;
  //Loop until we find the appropriate number of loop ending instructions (])

  while (depth > 0)
  {
    _instructionptr++;
    if (_instructionptr >= _instructions.size())
    {
      Error("Unterminated loop");
      return false;
    }
    if (_instructions[_instructionptr] == '[')
    {
      depth++;
    }
    else if (_instructions[_instructionptr] == ']')
    {
      depth--;
    }
  }
  return true;
}

bool Brainfuck::ExecuteNext()
{
  char op = _instructions[_instructionptr];
  if (op == '>' || op == '<')//Move memory pointer
  {
    if (!MoveMemptr(op == '>'))
    {
      return false;
    }
  }
  else if (op == '+')//Increment current memory pointer
  {
    _memory[_memptr]++;
  }
  else if (op == '-')//Decrement current memory pointer
  {
    _memory[_memptr]--;
  }
  else if (op == '.')//Print current pointer
  {
    _output << _memory[_memptr];
  }
  else if (op == ',')//Read from user input
  {
    _input >> _memory[_memptr];
  }
  else if (op == '[')//Enter bloc if current pointer is non zero
  {
    if (_memory[_memptr] != 0)
    {
      _stack.push(_instructionptr);
    }
    else
    {
      if (!FindLoopEnd())
      {
        return false;
      }
    }
  }
  else if (op == ']')//End a loop, go back to loop begin
  {
    if (_stack.empty())
    {
      Error("Ending a loop while not in a loop");
      return false;
    }
    _instructionptr = _stack.top();
    _stack.pop();
    return true;
  }
  else
  {
    Error("Invalid instruction: \"" + string(&op, &op + 1) + "\"");
    return false;
  }
  _instructionptr++;
  return true;
}

bool Brainfuck::ExtendMemory()
{
  if (_maxmem != 0 && _memory.size() >= _maxmem)
  {
    Error("Program out of memory (needs more than " + std::to_string(_maxmem)+ " bytes)");
    return false;
  }

  _memory.resize(_memory.size() * 2);
  return true;
}

void Brainfuck::Error(const string &message)
{
  _errorstream << "Fatal: " << message << endl;
  _errorstream << "At code position: " << _instructionptr << ", at memory position: " << _memptr << endl;
}