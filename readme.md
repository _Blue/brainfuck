#Brainfuck interpreter

This repository contains a brainfuck interpreter written in C++14.

##Build

###Linux
To build the brainfuck binary, run `make` at the root of the repository .

*If you want to start the test suite, run `make check`.*

###Windows
* Open `win32/Brainfuck.sln` with Microsoft Visual Studio (you need at least the 2013 version)  
* Then build the `Release` target

*If you prefer using a terminal, you can also run `msbuild` in the `win32` folder to build the binary*

##Usage
Usage: brainfuck \[-h\] \[-m memory\] \[-f file\]
Options:

  * -m `count`  
      Allow the brainfuck program to use at most count bytes of memory
  * -f `file`  
      Reads brainfuck code from `file` instead of stdin
  * -h  
      Displays a help message

*Without options, brainfuck will read code from stdin*

##Return values
brainfuck can return one of the following values:

* 0 if everything went fine
* 1 if the argument syntax is incorrect or the specified file couldn't be opened
* 2 if an error occurred during the execution, such has program out of memory or invalid instruction

#License
You are allowed (and even encouraged) to use, modify and redistribute the content of this repository as you wish.
