CXXFLAGS = -std=c++14 -Wall -Wextra
BIN = brainfuck
SRC = Brainfuck main
OBJ = $(addprefix src/,$(addsuffix .o,$(SRC)))


all: CXXFLAGS += -O2
all: $(OBJ)
	$(CXX) $(OBJ) $(CXXFLAGS) -o $(BIN)

debug: CXXFLAGS+= -g3
debug: $(OBJ) all
	$(CXX) $(OBJ) $(CXXFLAGS) -o $(BIN)

check: CXXFLAGS += -DRUN_TEST
check: clean $(OBJ) src/Test.o
	$(CXX) $(CXXFLAGS) -o check $(OBJ) src/Test.o
	./check

clean:
	$(RM) $(OBJ) $(BIN) src/Test.o check
